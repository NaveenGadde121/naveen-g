#include<stdio.h>
struct Fract
{
    int num;
    int deno;
};
typedef struct Fract fract;
struct  Egyptian
{
    int m;
    int deno[100];
    fract sum;
}; 
typedef struct Egyptian egypt;
void input_1(egypt *one)
{
    scanf("%d",&one->m);
       for(int i=0;i<one->m;i++)
              scanf("%d",&one->deno[i]);
}
void input_n(int n,egypt Negypt[n])
{
    for(int i=0;i<n;i++)
              input_1(&Negypt[i]);
}
int gcd(int a,int b)
{
    a=a<b?a:b;
    if(b%a==0)
       return a;
       for(int i=a/2;i>1;i--)
       {
              if(a%i==0&&b%i==0)
                     return i;
       }
       return 1;
}
void compute_1(egypt *one)
{
    fract f;
       f.num=0;
       f.deno=1;
       int Gcd;
    for(int i=0;i<one->m;i++)
       {
        one->sum.num=(f.num*one->deno[i])+f.deno;
              one->sum.deno=f.deno*one->deno[i];
              Gcd=gcd(one->sum.num,one->sum.deno);
              one->sum.num=one->sum.num/Gcd;
              one->sum.deno=one->sum.deno/Gcd;
        f=one->sum;
       }
}
void compute(int n,egypt Negypt[n])
{
    for(int i=0;i<n;i++)
              compute_1(&Negypt[i]);
}
void output_1(egypt *one)
{
    for(int i=0;i<(one->m)-1;i++)
              printf("1/%d+",one->deno[i]);
       printf("1/%d=%d/%d\n",one->deno[(one->m)-1],one->sum.num,one->sum.deno);
}
void output_n(int n,egypt Negypt[n])
{
    for(int i=0;i<n;i++)
        output_1(&Negypt[i]);
}
int main()
{
       int n;
       scanf("%d",&n);
       egypt Negypt[n];
       input_n(n,Negypt);
       compute(n,Negypt);
       output_n(n,Negypt);
       return 0;
}