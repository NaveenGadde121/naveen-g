#include <stdio.h>

struct fraction
{
 int num;
 int deno;
};
typedef struct fraction Frac;

Frac input();
Frac sum(Frac x1, Frac x2);
int gcd(int m, int n);
Frac compute();
int output(Frac sum);

 

int gcd(int m, int n)
{
 int i,flag;
 for(i = 1; i <= m && i <= n; i++)
   {
       if(m % i == 0 && n % i == 0)
           flag = i;
   }
   return flag;
}
 
Frac sum(Frac x1, Frac x2)
{
 Frac x3;
 int c;
 x3.num = ((x1.num*x2.deno)+ (x2.num*x1.deno));
 x3.deno = x1.deno*x2.deno;
 c = gcd(x3.num,x3.deno);
 x3.num = x3.num/c;
 x3.deno = x3.deno/c;
 return x3;
}
 
Frac compute()
{
 int fractions,i;
 Frac flag,z;
 printf("Enter the number of Fractions to Add:\n");
 scanf("%d",&fractions);
 Frac f[fractions];
 for(i=0;i<fractions;i++)
{
   printf("Now enter the fraction %d\n",i+1);
   f[i] = input();
 }
 if(fractions==1)
{
   z = f[0];
 }
 else{
   flag = f[0];
   for(i=0;i<fractions-1;i++)
{
     z = sum(flag,f[i+1]);
     flag = z;
   }
 }
 return z;
}
 
int output(Frac sum)
{
 printf("The sum of the given fractions : %d/ %d",sum.num,sum.deno);
 return 0;
}

int main()
{
 Frac flag;
 flag = compute();
 output(flag);
 return 0;
}
 
Frac input()
{
 Frac f;
 printf("Enter the numerator:\n ");
 scanf("%d",&f.num);
 printf("Enter the denominator:\n ");
 scanf("%d",&f.deno);
 return f;
}